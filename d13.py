wait = 1003681
buses = "23,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,37,x,x,x,x,x,431,x,x,x,x,x,x,x,x,x,x,x,x,13,17,x,x,x,x,19,x,x,x,x,x,x,x,x,x,x,x,409,x,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,29"
buses = buses.strip().split(sep=',')
buses = {i: int(buses[i]) for i in range(len(buses)) if buses[i] != 'x'}

bs = {(x:=((wait//bus) + 1)*bus):(x - wait)*bus for bus in buses.values()}
bss = min(bs.keys())
print(bs[bss])


from functools import reduce
modulos = list(buses.values())
M=reduce(lambda x, y: x*y, modulos)
Mi = [M // ai for ai in modulos]
Minv = [0] + [pow(Mi[i], -1, modulos[i]) for i in range(1, len(modulos))]
S = sum([list(buses.keys())[i] * Mi[i] * Minv[i] for i in range(len(modulos))]) % M
print(M - S)