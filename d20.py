import os
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

big_tiles = {}
with open(os.path.join(os.getcwd(), r"d20.txt")) as f:
    BT = 0
    for line in f:
        if (l:=line.strip()) == '':
            pass
        elif l[0] == 'T':
            BT = int(l[4:-1])
            big_tiles[BT] = []
        else:
            big_tiles[BT].append(l)

edges = {}
fedges = {}
for tile in big_tiles:
    # big_tiles[tile] = aoc.Maze(big_tiles[tile])
    t = np.array([[1 if x == '#' else 0 for x in line] for line in big_tiles[tile]])
    edges[tile] = [ t[0,:], t[0,::-1], t[len(t)-1,:],t[len(t)-1,::-1],
                    t[:,0], t[::-1,0], t[:,len(t[0])-1],t[::-1,len(t[0])-1]]
    for edge in edges[tile]:
        k = tuple(edge)
        if k in fedges.keys():
            fedges[k] += [tile]
        else:
            fedges[k] = [tile]
uniq = {}

from collections import Counter
C=Counter([x[0] for x in fedges.values() if len(x) == 1])
C = [c for c in C if C[c] == 4]
print(C)
M = 1
for c in C:
    M *= c
print(M)


S = {}
for x in fedges:
    if len(fedges[x]) > 1:
        if (s:=tuple(fedges[x])) in S.keys():
            S[s] += [x]
        else:
            S[s] = [x]
# print(S)

# region hide
# G = nx.MultiGraph()
# G.add_edges_from(S)
# nx.draw(
#             G,  
#             with_labels = True, 
#             node_size = 200, 
#             node_color = 'r', 
#             # cmap = 'paired',
#             font_size = 10,
#             font_weight = 'bold',
#             font_color = 'k'
#         )

# plt.show()

# visited_edges = []
# edges_to_do = G[C[0]]
# endregion 

def multidim_intersect(arr1, arr2):
    arr1_view = arr1.view([('',arr1.dtype)]*arr1.shape[1])
    arr2_view = arr2.view([('',arr2.dtype)]*arr2.shape[1])
    intersected = np.intersect1d(arr1_view, arr2_view)
    return intersected.view(arr1.dtype).reshape(-1, arr1.shape[1])

T_conn = {}
for edge in fedges:
    for sedge in fedges[edge]:
        if sedge in T_conn.keys():
            T_conn[sedge] += [edge]
        else:
            T_conn[sedge] = [edge]

T = {tile:np.array([[1 if x == '#' else 0 for x in line] for line in big_tiles[tile]]) for tile in big_tiles}
for t1 in T:
    for t2 in T:
        if t1 is not t2:
            tt2 = T[t2]
            tt2f = np.flip(tt2)
            for t2m in [tt2, np.rot90(tt2,1), np.rot90(tt2, 2), np.rot90(tt2, 3),
                        tt2f, np.rot90(tt2f,1), np.rot90(tt2f, 2), np.rot90(tt2f, 3)]:
                intersection = multidim_intersect(T[t1], t2m)
                if len(intersection) == 0:
                    continue
                #TODO add intersection data
                break
            else:
                break