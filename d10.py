import os

from networkx.algorithms.isolate import isolates 

with open(os.path.join(os.getcwd(), r"d10.txt")) as f:
    adapters = sorted([int(line.strip()) for line in f])
    adapters = [0] + adapters + [adapters[-1] + 3]
    
    # part 1
    diff = []
    for i in range(1, len(adapters)):
        diff.append(adapters[i] - adapters[i-1])
    print(diff.count(3), diff.count(1), diff.count(3)*diff.count(1))
    

    
    # print graph
    import networkx as nx
    from networkx.algorithms import bridges
    from operator import mul
    from functools import reduce
    import matplotlib.pyplot as plt
    
    G = nx.DiGraph()
    [G.add_edge(adapters[i], adapters[i + j], weight = j) 
        for i in range(len(adapters)) for j in range(1, 4) 
        if i+j < len(adapters) and adapters[i+j]-adapters[i] <= 3]
    
    
    fig, axes = plt.subplots(ncols=2)
    nx.draw(G, pos=nx.spiral_layout(G), with_labels = True, ax=axes[0])
    
    G.remove_edges_from(bridges(G.to_undirected()))
    G.remove_nodes_from(list(isolates(G)))
    print(reduce(mul, [sum([1 for _ in nx.all_simple_paths(G, min(list(subG)), max(list(subG)))]) 
                        for subG in nx.connected_components(G.to_undirected())], 1))
    
    nx.draw(G, pos=nx.spiral_layout(G), with_labels = True, ax=axes[1])
    
    plt.show()