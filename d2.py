import re
import os

rx = re.compile(r'(\d+)-(\d+) (\w): (\w+)')

part1 = 0
part2 = 0
with open(os.path.join(os.getcwd(), r"d2.txt")) as f:
    for line in f:
        fr, to, char, string = rx.findall(line.strip())[0]
        fr, to = int(fr), int(to)
        if fr <= string.count(char) <= to:
            part1 += 1

        if (string[fr - 1] == char != string[to - 1] or 
            string[fr - 1] != char == string[to - 1]):
                part2 += 1
            
print(part1)
print(part2)