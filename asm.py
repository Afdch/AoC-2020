class computer:
    def __init__(self) -> None:
        self.accumulator = 0
        self.pointer = 0
        self.instruction_list = []
        
    def __is_int(self, x: str) -> bool:
        try:
            int(x)
            return True
        except ValueError:
            return False

    def read_instruction_list(self, f) -> None:
        self.instruction_list = [(ins[0], [int(x) if self.__is_int(x) else x for x in ins[1:]]) 
                                for ins in [line.strip().split() for line in f]]
        return None

    def get_instruction(self):
        return self.instruction_list[self.pointer]
    
    def exec_instr(self):
        instruction, arguments = self.instruction_list[self.pointer]
        if instruction == 'nop':
            self.pointer += 1
        elif instruction == 'jmp':
            self.pointer += arguments[0]
        elif instruction == 'acc':
            self.accumulator += arguments[0]
            self.pointer += 1