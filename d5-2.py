import os
from time import time

# an alternative solution based on Marvin's idea
t = time()
with open(os.path.join(os.getcwd(), r"d5.txt")) as f:
    seats = sorted([int(line.strip().replace('F', '0').replace('B', '1').replace('R', '1').replace('L', '0'), 2) for line in f])
    print(seats[-1])
    print(time() - t)
    for i in range(len(seats)):
        if seats[i] + 2 == seats[i + 1]:
            print(seats[i] + 1)
            print(time() - t)
            break