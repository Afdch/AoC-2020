import os, copy

P = [[],[]]
with open(os.path.join(os.getcwd(), r"d22.txt")) as f:
    for line in f:
        if (l := line.strip()) == 'Player 1:' or l =='':
            p = 0
        elif l == 'Player 2:':
            p += 1
        else:
            P[p] += [int(l.strip())]

def score(Pi):
    print(sum([(c+1)*p for c, p in enumerate(reversed(Pi))]))

def part1(P):
    while all([p for p in P]):
        c = [p.pop(0) for p in P]
        P[0 if (C:=c[0] > c[1]) else 1] += c if C else reversed(c)
    else:
        score(P[0] if P[0] else P[1])
        
def part2(P, it = 0):
    game_states = []
    while all([p for p in P]):
        if (S:=tuple([tuple(p) for p in P])) in game_states: # check for infinite loops:
            break 
        game_states.append(S)
        c = [p.pop(0) for p in P]
        if all([len(P[i]) >= c[i] for i in range(2)]): #recursive game
            P[(pp:=part2([P[i][:c[p]] for i in range(2)], it + 1))] += c if pp == 0 else reversed(c)
        else:
            P[0 if (C:=c[0] > c[1]) else 1] += c if C else reversed(c)
    score(P[0] if P[0] else P[1]) if it == 0 else None
    return 0 if P[0] else 1

part1(copy.deepcopy(P))
part2(copy.deepcopy(P))