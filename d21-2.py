import os, re, itertools
import networkx as nx
import matplotlib.pyplot as plt
from collections import defaultdict
all_allergens = defaultdict(list)
with open(os.path.join(os.getcwd(), r"d21.txt")) as f:
    [[all_allergens[allergen].append(re.compile(r'([\w ]+) \(').findall(line)[0].split(' ')) for allergen in re.compile(r'\(contains ([\w ,]+)\)').findall(line)[0].split(', ')] for line in f]

all_allergens = {key: set.intersection(*[set(x) for x in all_allergens[key]]) for key in all_allergens}
G = nx.DiGraph(all_allergens)
P = nx.algorithms.bipartite.matching.hopcroft_karp_matching(G, all_allergens)
z =sum([1 for x in list(itertools.chain.from_iterable(all_allergens.values())) if x not in P.values()])
print(f"Parg 1 answer is {z}")
P = [(key, P[key]) for key in P if key in all_allergens]
P = nx.DiGraph(P)
# nx.draw(G,with_labels = True)
#nx.draw(nx.DiGraph([(x, P[x]) for x in P if x in all_allergens]),with_labels = True,pos=nx.drawing.layout.bipartite_layout(P, all_allergens.keys()))
pos=nx.drawing.layout.bipartite_layout(G, all_allergens.keys())
nx.draw(G,with_labels = True,pos = pos)
nx.draw(P, pos=pos, node_color = 'g',)
nx.draw_networkx_edges(P, pos, width=3, edge_color='r')
plt.show()