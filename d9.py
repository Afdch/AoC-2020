from collections import deque
import os

preamble = 25

numbers = deque(maxlen = preamble)

def number_in_numbers(number):
    for x in numbers:
        for y in numbers:
            if x + y == number:
                return True
    else:
        return False
all_numbers = []
with open(os.path.join(os.getcwd(), r"d9.txt")) as f:
    for line in f:
        all_numbers.append(int(line.strip()))


# part 1
part1_number = 0
for number in all_numbers:
    if len(numbers) < preamble or number_in_numbers(number):
        numbers.append(number)
    else:
        part1_number = number
        print(f'Part 1 answer is {number}')
        break
    

# part 2
valid_numbers = [x for x in all_numbers if x < part1_number]
len_num = len(valid_numbers)
print(f"total valid numbers = {len_num}")
from itertools import accumulate


# I still don't know if it should be range(1, len_num)
for i in range(len_num):  
    if part1_number in (a := accumulate(valid_numbers[i:])):
        k = len_num - len(list(a)) - 1
        print(max(b := valid_numbers[i:k]), min(b), max(b) + min(b))
        break
else:
    print('w0t')