import queue
from matplotlib.pyplot import xcorr

from networkx.algorithms.shortest_paths import weighted


class Maze:
    """
    An AoC standard Maze class used to store and find a path within the maze
    
    Attributes
    ----------
    tiles: dict
        dictionary keys in (x, y) format; value is a character displayed
        
    walls: list
        a list of wall tiles in the maze
        
    objects: list
        a list of coordinates of non-wall non-empty tiles in the maze

    """
    
    def __init__(self, file, walltile = "#", emptytile = ".") -> None:
        self.tiles = self.__readmaze(file)
        self.maxX = max([x for x, _ in self.tiles.keys()])
        self.maxY = max([y for _, y in self.tiles.keys()])
        self.walls = [x for x in self.tiles.keys() if self.tiles[x] == walltile]
        self.objects = [x for x in self.tiles.keys() if self.tiles[x] not in [emptytile, walltile]]
    
    #region boringmethods    
    def __readmaze(self, f):
        """Specify an AoC standard maze file read with open()"""
        fullmaze = {}
        j = 0
        for line in f:
            i = 0
            for char in line.strip():
                fullmaze[(i, j)] = char
                i += 1
            j += 1
        return fullmaze        
    
    def isWall(self, tile):
        return tile in self.walls
    
    def isEmptyorObject(self, tile):
        return tile not in self.walls
    #endregion
    
    #region fancymethods
    def neighbours(self, tile):
        x, y = tile
        nei = []
        for x1, y1 in [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]:
            if 0 <= x1 <= self.maxX and 0 <= y1 <= self.maxY:
                if (x1, y1) not in self.walls:
                    nei.append((x1, y1))
        return nei
        
    def __heuristic(self, a:tuple, b:tuple):
        return abs(a[0] - b[0]) + abs(a[1] - b[1])
    
    def reduce_maze(self):
        deadends = queue.Queue()
        for tile in self.tiles:
            if tile not in self.walls:
                neis = self.neighbours(tile)
                if len(neis) == 1:
                    deadends.put(tile)
        
        while not deadends.empty():
            tile = deadends.get()
            if tile not in self.objects:                
                n = self.neighbours(tile)
                if len(n) == 1:
                    deadends.put(n[0])
                    self.tiles[tile] = '#'
                    self.walls.append(tile)
        else:
            print('done')
                
    
    def findPath(self, source:tuple, destination:tuple, mode:str = 'distance'):
        """A* path from source to destination"""
        from queue import PriorityQueue
        cost = {source: 0}
        frontier = PriorityQueue()
        for tile in self.neighbours(source):
            frontier.put((1, tile))
            cost[tile] = 1
        
        while not frontier.empty():
            _, current = frontier.get()
            
            if current == destination:
                if mode == 'distance':
                    return cost[current]
                else:
                    # TODO return path
                    # possible modes:
                    # distance -> int value (default)
                    # path -> a list of steps from _source_ to _destination_
                    return cost[current]
            
            for next in self.neighbours(current):
                next_cost = cost[current] + 1
                if next not in cost or next_cost < cost[current]:
                    cost[next] = next_cost
                    priority = next_cost + self.__heuristic(current, next)
                    frontier.put((priority, next))
    #endregion
    
    #region graphs
    def build_graph(self, show = False):
        """
        Builds a full graph of the maze. 
        vertices are any intersections and objects within the maze
        edges meause distance between the vertices
        
        networkx compatible
        """
        import networkx as nx
        self.graph = nx.Graph()
        # 1. select a random point from objects
        
        if self.objects:
            start = self.objects[0]
        else:
            return None
        
        self.graph.add_node(start)
        # frontier = queue.Queue()
        # frontier.put((start, start, 0))
        frontier = []
        frontier.append((start, start, 0))
        visited = set([(start)])
        
        while len(frontier) > 0:
        # while not frontier.empty():
            # last_node, current, cost = frontier.get()
            last_node, current, cost = frontier.pop()
            neighs = self.neighbours(current)
            if current not in self.objects:
                visited |= set([current])
            if ((len(neighs) > 2 or current in self.objects) 
                and (last_node != current or cost > 1 )):
                # A = self.tiles[last_node] if last_node in self.objects else last_node
                # B = self.tiles[current] if current in self.objects else current
                
                # self.graph.add_node(A, pos = current)
                # self.graph.add_node(B, pos = last_node)
                # self.graph.add_edge(A, B, weight=cost)
                self.graph.add_edge(last_node, current, weight=cost)
                cost = 0
                last_node = current
            for next in neighs:
                if next not in visited:
                    # frontier.put((last_node, next, cost + 1))
                    frontier.append((last_node, next, cost + 1))
        
        if False:
            self.graph_reduce()
            
        if show:
            self.show_graph()
    
    def graph_reduce(self):
        import networkx as nx
        delete_nodes = []
        make_neighbours = []
        for node in nx.nodes(self.graph):
            if node not in self.objects:
                neighbours = list(nx.neighbors(self.graph, node))
                if len(neighbours) == 2:
                    delete_nodes.append(node)
                    make_neighbours.append(neighbours)
        else:
            for neighbours in make_neighbours:
                self.graph.add_edge(neighbours[0], neighbours[1])
            for node in delete_nodes:
                self.graph.remove_node(node)
                
    
    def show_graph(self, node_pos = None, node_labels = None, node_colors = None):
        #TODO: stylize
        from matplotlib import cm
        import matplotlib.pyplot as plt
        import networkx as nx
        
        node_pos = {x:x for x in self.tiles} if node_pos is None else node_pos
        node_labels = {x: self.tiles[x] if x in self.objects else '' for x in self.tiles}
        edge_labels = nx.get_edge_attributes(self.graph, 'weight')
        
        nx.draw(
            self.graph, 
            pos= node_pos, 
            with_labels = True, 
            node_size = 200, 
            # node_color = '#530aab', 
            # cmap = 'paired',
            labels = node_labels,
            font_size = 10,
            font_weight = 'bold',
            font_color = '#aaa'
        )

        nx.draw_networkx_edge_labels(
            self.graph, 
            pos = node_pos,
            edge_labels = edge_labels
        )
        
        plt.gca().invert_yaxis()
        plt.show()
    #endregion