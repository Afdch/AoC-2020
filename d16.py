import os


with open(os.path.join(os.getcwd(), r"d16.txt")) as f:
    rules, your_ticket, near_tickets = "".join([line for line in f]).split('\n\n')
    rules = {rule[:rule.index(':')]:[[int(x[0]) , int(x[1])] for x in [r.split('-') for r in rule[rule.index(":")+1:].split(' or ')]] for rule in rules.split('\n')}
    your_ticket = [int(x) for x in ((your_ticket.split('\n'))[1]).split(',')]
    near_tickets = [[int(x) for x in ticket.split(',')] for ticket in (near_tickets.split('\n'))[1:]]
    
    # print(n_rules, your_ticket, near_tickets)
    valid_ints = []
    for r in rules.values():
        valid_ints += [x for x in range(r[0][0], r[0][1] + 1)]
        valid_ints += [x for x in range(r[1][0], r[1][1] + 1)]
    errors = 0
    invalid_tickets = []
    for ticket in near_tickets:
        for field in ticket:
            if field not in valid_ints:
                errors += field
                invalid_tickets.append(ticket)
    #part 1
    print(errors)
    
    #part 2
    near_tickets = [x for x in near_tickets if x not in invalid_tickets]
    ticket_rule_groups = [tuple([ticket[i] for ticket in near_tickets]) for i in range(len(rules.values()))]
    
    valid_groups_for_rule = {x: set() for x in rules.keys()}
    for rule in rules.keys():
        r = rules[rule]
        vr = [x for x in range(r[0][0], r[0][1] + 1)] + [x for x in range(r[1][0], r[1][1] + 1)]
        for gr in ticket_rule_groups:
            if all([x in vr for x in gr]):
                valid_groups_for_rule[rule].add(tuple(gr))
    # print(valid_groups_for_rule)
    
    verified_rules = {}
    while len(verified_rules) < len(valid_groups_for_rule.keys()):
        for rule in [x for x in valid_groups_for_rule.keys() if x not in verified_rules.keys()]:
            if len(valid_groups_for_rule[rule]) == 1:
                verified_rules[rule] = valid_groups_for_rule[rule]
        for rule in [x for x in valid_groups_for_rule.keys() if x not in verified_rules.keys()]:
            for r in verified_rules.keys():
                valid_groups_for_rule[rule] -= verified_rules[r]

    # print(verified_rules)
    
    from operator import mul
    from functools import reduce
    print(reduce(mul, [your_ticket[ticket_rule_groups.index(list(verified_rules[rule])[0])] for rule in [x for x in verified_rules if 'departure' in x]], 1))
