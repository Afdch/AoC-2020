from aoc import Maze
import os

with open(os.path.join(os.getcwd(), r"test.txt")) as f:
    maze = Maze(f)
    
maze.reduce_maze()
# for y in range(maze.maxY + 1):
#     print(''.join(maze.tiles[(x, y)] for x in range(maze.maxX + 1)))
maze.build_graph(show = True)

