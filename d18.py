import os
import re

op = []
with open(os.path.join(os.getcwd(), r"d18.txt")) as f:
    for line in f:
        op.append(line.strip())
        # r = list(re.finditer(r'\d', line))
        # r = [x.span()[0] for x in r]
        # for i in range(len(r) - 1):
        #     pass

# part1
def resolve_string(st, r):
    # gr = [x.span() for x in re.finditer(r'(\([\d +*]+\))', st)]
    gr = []
    op = 0
    g = 0
    for c in range(len(st)):
        if (ch := st[c]) == '(':
            if op == 0:
                g = c
            op += 1
        elif ch == ')':
            op -= 1
            if op == 0:
                gr.append((g, c + 1))
        
    repl = [resolve_string(st[g[0]+1:g[1]-1]) for g in gr]
    for i in range(len(gr) - 1, -1, -1):
        g = gr[i]
        st = st[:g[0]] + repl[i] + st[g[1]:]
        
    while (s := re.search(r'(\d+ [+*] \d+)', st)) is not None:
        t = s.span()
        st = str(eval(st[t[0]:t[1]])) + st[t[1]:]
        
    return st

# part2
def resolve_string2(st):
    # gr = [x.span() for x in re.finditer(r'(\([\d +*]+\))', st)]
    gr = []
    op = 0
    g = 0
    for c in range(len(st)):
        if (ch := st[c]) == '(':
            if op == 0:
                g = c
            op += 1
        elif ch == ')':
            op -= 1
            if op == 0:
                gr.append((g, c + 1))
        
    repl = [resolve_string2(st[g[0]+1:g[1]-1]) for g in gr]
    for i in range(len(gr) - 1, -1, -1):
        g = gr[i]
        st = st[:g[0]] + repl[i] + st[g[1]:]     
        
        
    while (s := re.search(r'(\d+ [+] \d+)', st)) is not None:
        t = s.span()
        st = st[:t[0]] + str(eval(st[t[0]:t[1]])) + st[t[1]:]
        
    while (s := re.search(r'(\d+ [*] \d+)', st)) is not None:
        t = s.span()
        st = st[:t[0]] + str(eval(st[t[0]:t[1]])) + st[t[1]:]
        
    return st


print(sum([int(resolve_string(line)) for line in op]))
print(sum([int(resolve_string2(line)) for line in op]))