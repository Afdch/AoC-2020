import os

def part1():
    position = 0 + 0j
    rotation = 1
    with open(os.path.join(os.getcwd(), r"d12.txt")) as f:
        for line in f:
            ins = line.strip()
            dir = ins[0]
            am = int(ins[1:])
            if dir == 'N':
                position += am * 1j
            elif dir == 'S':
                position -= am * 1j
            elif dir == 'E':
                position += am
            elif dir == 'W':
                position -= am
            elif dir == 'L':
                rotation = rotation * (1j ** (am // 90))
            elif dir == 'R':
                rotation = rotation / (1j ** (am // 90))
            elif dir == 'F':
                position += rotation * am
        print(abs(position.real) + abs(position.imag))


def part2():
    position = 0 + 0j
    rotation = 10 + 1j
    with open(os.path.join(os.getcwd(), r"d12.txt")) as f:
        for line in f:
            ins = line.strip()
            dir = ins[0]
            am = int(ins[1:])
            if dir == 'N':
                rotation += am * 1j
            elif dir == 'S':
                rotation -= am * 1j
            elif dir == 'E':
                rotation += am
            elif dir == 'W':
                rotation -= am
            elif dir == 'L':
                rotation = rotation * (1j ** (am // 90))
            elif dir == 'R':
                rotation = rotation / (1j ** (am // 90))
            elif dir == 'F':
                position += rotation * am
        print(abs(position.real) + abs(position.imag))
        
part2()