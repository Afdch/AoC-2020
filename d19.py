import os
import re

rules = {}
lines = []

def is_int(x) -> bool:
    try:
        int(x)
        return True
    except ValueError:
        return False

with open(os.path.join(os.getcwd(), r"d19.txt")) as f:
    le = False
    for line in f:
        l = line.strip()
        if not le:
            if not l == "":
                a, b = l.split(':')
                rules[int(a)] = b[1:].replace('"','')
                continue
            else:
                le = True
                continue
            
        lines.append(l)

# print(rules)
# print(lines)
# print()

only_str = []
while True:
    have_ints = 0
    for rule in rules:
        if rule not in only_str:
            tk = rules[rule].replace('|', ' ').replace('(', ' ').replace(')', ' ').split(' ')
            if sum(lst:=set([int(x) for x in tk if is_int(x)])) > 0:
                have_ints += 1
                for i in [x for x in lst if x in only_str]:
                    
                    subrules = rules[rule].split('|')
                    
                    r = rules[rule].replace(str(i), '(?:' + rules[i].replace(' ', '') +')')
                    while (x:=re.compile(r'\(\?:([ab]+?)\)').search(r)) is not None:
                        r = r[:x.regs[0][0]] + r[x.regs[1][0]:x.regs[1][1]] + r[x.regs[0][1]:]
                    rules[rule] = r
            else:
                only_str.append(rule)

    # print(only_str)
    if have_ints == 0:
        break
# r = '^' + rules[0].replace(' ', '').replace('(?:a)', 'a').replace('(?:b)', 'b') + '$'
r = '^' + rules[0].replace(' ', '') + '$'

# remove all not needed groups
while (x:=re.compile(r'\(\?:([ab]+?)\)').search(r)) is not None:
    #y doesn't this work
    #r = re.sub(x[0], x[1], r)
    
    r = r[:x.regs[0][0]] + r[x.regs[1][0]:x.regs[1][1]] + r[x.regs[0][1]:]
rg = re.compile(r)

print(r)

# for rule in rules:
#     print(rule, rules[rule])

print(sum([1 for x in lines if rg.match(x) is not None]))
# 83 is not the right answer
# 87 is not the right answer

# nitro 279