def crab_game(part2):
    c = [int(x) for x in '157623984']
    cups = {c[i]: c[(i+1) % len(c)] for i in range(len(c))}
    if part2:
        cups.update({c[-1]:10})
        cups.update({x:x+1 for x in range(10, 1000000)})
        cups[1000000] = c[0]

    current = c[0]    
    for _ in range(10000000) if part2 else range(100):
        pick_up1 = cups[current]
        pick_up2 = cups[pick_up1]
        pick_up3 = cups[pick_up2]
        destination = current - 1
        while destination in [pick_up1, pick_up2, pick_up3] or destination == 0:
            if destination == 0:
                destination = max(cups.keys())
                continue
            destination -= 1
        cups[current] = cups[pick_up3]
        cups[pick_up3] = cups[destination]
        cups[destination] = pick_up1
        current = cups[current]
    if part2:
        print(cups[1] * cups[cups[1]])
    else:
        lst = [1]
        for i in range(1, len(cups)):
            lst.append(cups[lst[-1]])
        print("".join([str(x) for x in lst[1:]]))    

crab_game(False)
crab_game(True)