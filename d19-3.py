import os
import time
t = time.time()
rules = {}
lines = []

def is_int(x) -> bool:
    try:
        int(x)
        return True
    except ValueError:
        return False

with open(os.path.join(os.getcwd(), r"d19.txt")) as f:
    le = False
    for line in f:
        l = line.strip()
        if not le:
            if not l == "":
                a, b = l.split(':')
                b = b[1:].replace('"','')
                if b in ['a', 'b']:
                    rules[int(a)] = b
                else:
                    rules[int(a)] = [tuple([int(y) for y in x.split(' ') if is_int(y)]) for x in b.split('|')]
                    # rules[int(a)] = [int(x) for x in [y.split() for y in b[1:].replace('"','').split('|')] if is_int(x)]
                    if len(rules[int(a)]) == 1:
                        rules[int(a)] = rules[int(a)][0]
                continue
            else:
                le = True
                continue
            
        lines.append(l)

def go_deeper(rule):
    ret = []
    if type(rule) is str:
        return rule
    if type(rule) is int:
        return go_deeper(rules[rule])
    if type(rule) is list:
        return [go_deeper(r) for r in rule]
    if type(rule) is tuple:
        return tuple([go_deeper(r) for r in rule])

P42 = go_deeper(42)
# print(P42)

def mk_d(P):
    dictionary = [P]
    while True:
        if all([all([type(x) is str for x in y]) for y in dictionary]):
            break
        term = dictionary.pop(0)
        
        if type(term) is list:
            for p in term:
                dictionary.append(p)
            continue
        for i in range(len(term)):
            tm = list(term)
            if type(tm[i]) is list:
                for item in tm[i]:
                    new_var = tm[:i] + [item] + tm[i+1:]
                    dictionary.append(tuple(new_var))
                break
            elif type(tm[i]) is tuple:
                dictionary.append(tuple(tm[:i] + list(tm[i]) + tm[i+1:]))
                break
        else:
            dictionary.append(term)
    for i in range(len(dictionary)):
        dictionary[i] = ''.join(dictionary[i])
    return set(dictionary)
D = {}
D[42] = mk_d(P42)
D[31] = mk_d(go_deeper(31))



total_match = 0
possible = [(8, 11)]
all_comb = set()
valid_comb = set()

# for part 2
while len(possible) > 0:
    comb = possible.pop(0)
    if all([x not in [8, 11] for x in comb]):
        valid_comb |= set(tuple([comb]))
    all_comb |= set(tuple([comb]))
    comb = list(comb)
    for i in range(len(comb)):
        if comb[i] == 8:
            new_comb = tuple(comb[:i] + [42] + comb[i + 1:])
            if len([x for x in new_comb if x not in [8, 11]]) <= 11 and new_comb not in all_comb:
                possible.append(new_comb)
            new_comb = tuple(comb[:i] + [42, 8] + comb[i + 1:])
            if len([x for x in new_comb if x not in [8, 11]]) <= 11 and new_comb not in all_comb:
                possible.append(new_comb)
        if comb[i] == 11:
            new_comb = tuple(comb[:i] + [42, 31] + comb[i + 1:])
            if len([x for x in new_comb if x not in [8, 11]]) <= 11 and new_comb not in all_comb:
                possible.append(new_comb)
            new_comb = tuple(comb[:i] + [42, 11, 31] + comb[i + 1:])
            if len([x for x in new_comb if x not in [8, 11]]) <= 11 and new_comb not in all_comb:
                possible.append(new_comb)
            
            
            
for element in lines:
    # part 1
    # 42 42 31
    # c = [42, 42, 31]
    # valid_comb = [c]   
    for c in valid_comb:
        k = len(element) // 8
        if k != len(c) or len(element) % 8 != 0:
            continue
        if all([element[i*8:(i+1)*8] in D[c[i]] for i in range(k)]):
            total_match += 1

print(total_match)
print(time.time() - t)