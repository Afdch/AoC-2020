import re
import os

re_msk = re.compile(r'mask = ([X10]+)')
re_mem = re.compile(r'mem\[(\d+)\] = (\d+)')

def part1():
    mask = 0
    mem = {}
    with open(os.path.join(os.getcwd(), r"d14.txt")) as f:
        for line in f:
            if r:=re_msk.match(line.strip()):
                mask = r.group(1)
            elif r:=re_mem.match(line.strip()):
                value = int(r.group(2))
                b = bin(value)[2:]
                value = ""
                b = "0"*(len(mask) - len(b)) + b
                for bit in range(len(mask)):
                    if mask[bit] in ['1', '0']:
                        value += mask[bit]
                    else:
                        try:
                            value += b[bit]
                        except IndexError:
                            value += "0" 

                mem[int(r.group(1))] = int(value, 2)
                
        print(sum(list(mem.values())))

def part2():
    mask = 0
    mem = {}
    with open(os.path.join(os.getcwd(), r"d14.txt")) as f:
        for line in f:
            if r:=re_msk.match(line.strip()):
                mask = r.group(1)
            elif r:=re_mem.match(line.strip()):
                value = int(r.group(2))
                
                b_mem = bin(int(r.group(1)))[2:]
                addr = ""
                length = len(mask)
                b_mem = "0"*(length - len(b_mem)) + b_mem
                for bit in range(length):
                    if mask[bit] in ['1', 'X']:
                        addr += mask[bit]
                    elif mask[bit] == '0':
                        addr += b_mem[bit]
                
                addr = [addr]
                for bit in range(length):
                    new_addr = []
                    for ad in range(len(addr)):
                        if addr[ad][bit] == 'X':
                            new_addr.append(addr[ad][:bit] + "0" + addr[ad][bit + 1:])
                            new_addr.append(addr[ad][:bit] + "1" + addr[ad][bit + 1:])
                        else:
                            new_addr.append(addr[ad])
                    addr = new_addr
                for ad in addr:
                    mem[int(ad, 2)] = value

        print(sum(list(mem.values())))

# part1()
part2()