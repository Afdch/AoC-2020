import os
import re

#region open file
with open(os.path.join(os.getcwd(), r"d4.txt")) as f:
    passports = [{x.split(':')[0]:x.split(':')[1] for x in p.split(' ')} for p in [x.replace('\n', ' ') for x in ("".join([x for x in f])).split('\n\n')]]
#endregion

# regex solution just for memory

# all characters between 2 newlines:
# (?:.|(?<!\n)\n)+

# all characters that contains term. for multiple terms link them consecutively:
# (?=(?:.|(?<!\n)\n)*?term)(?:.|(?<!\n)\n)+

# conditions for each statement:
# byr (Birth Year) - four digits; at least 1920 and at most 2002.         (?=(?:.|(?<!\n)\n)*?byr:(?:19\d\d|200[0-2]))
# iyr (Issue Year) - four digits; at least 2010 and at most 2020.         (?=(?:.|(?<!\n)\n)*?iyr:(?:20(?:1[0-9]|20)))
# eyr (Expiration Year) - four digits; at least 2020 and at most 2030.    (?=(?:.|(?<!\n)\n)*?eyr:(?:20(?:2[0-9]|30)))
# hgt (Height) - a number followed by either cm or in:                    (?=(?:.|(?<!\n)\n)*?hgt:(?:1(?:5\d|6\d|7\d|8\d|9[0-3])cm|(?:59|6\d|7[0-6])in)\s)
# If cm, the number must be at least 150 and at most 193.                 
# If in, the number must be at least 59 and at most 76.
# hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.   (?=(?:.|(?<!\n)\n)*?hcl:#[0-9a-f]{6})(?!(?:.|(?<!\n)\n)*?hcl:#[0-9a-f]{7,})                                                                        
# ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.          (?=(?:.|(?<!\n)\n)*?ecl:(?:amb|blu|brn|gry|grn|hzl|oth))
# pid (Passport ID) - a nine-digit number, including leading zeroes.      (?=(?:.|(?<!\n)\n)*?pid:\d{9})(?!(?:.|(?<!\n)\n)*?pid:\d{10,})
# cid (Country ID) - ignored, missing or not.

# resulting regex
# (?=(?:.|(?<!\n)\n)*?byr:(?:19\d\d|200[0-2]))(?=(?:.|(?<!\n)\n)*?iyr:(?:20(?:1[0-9]|20)))(?=(?:.|(?<!\n)\n)*?eyr:(?:20(?:2[0-9]|30)))(?=(?:.|(?<!\n)\n)*?hgt:(?:1(?:5\d|6\d|7\d|8\d|9[0-3])cm|(?:59|6\d|7[0-6])in))(?=(?:.|(?<!\n)\n)*?hcl:#[0-9a-f]{6})(?!(?:.|(?<!\n)\n)*?hcl:#[0-9a-f]{7,})(?=(?:.|(?<!\n)\n)*?ecl:(?:amb|blu|brn|gry|grn|hzl|oth))(?=(?:.|(?<!\n)\n)*?pid:\d{9})(?!(?:.|(?<!\n)\n)*?pid:\d{10,})(?:.|(?<!\n)\n)+


def is_int(x) -> bool:
    try:
        int(x)
        return True
    except ValueError:
        return False

#region pats
def part1() -> int:
    valid = 0
    for p in passports:
        if len(p.keys()) == 8 or (len(p.keys()) == 7 and 'cid' not in p.keys()):
            valid += 1
    return valid

def part2() -> int:
    valid = 0
    for p in passports:
        if len(p.keys()) < 7 or (len(p.keys()) == 7 and 'cid' in p.keys()):
            continue
        if not is_int(p['byr']) or not (1920 <= int(p['byr']) <= 2002):
            continue
        if not is_int(p['iyr']) or not (2010 <= int(p['iyr']) <= 2020):
            continue
        if not is_int(p['eyr']) or not (2020 <= int(p['eyr']) <= 2030):
            continue
        m = re.compile(r'(\d{2,3})(cm|in)').match(p['hgt'])
        if m is None or (m[2] == 'in' and not (59 <= int(m[1]) <= 76) or
            m[2] == 'cm' and not (150 <= int(m[1]) <= 193)):
            continue
        if re.compile(r'#([\da-z]{6})').search(p['hcl']) is None:
            continue
        if p['ecl'] not in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
            continue
        id = p['pid']
        if len(id) != 9 or not is_int(id):
            continue
        valid += 1
    return valid


# print(part1())

print(part2())