import os
import re
from collections import defaultdict

all_allergens = defaultdict(list)
ingr_count = defaultdict(int)

r = re.compile(r'([\w ]+) \(contains ([\w ,]+)\)')
with open(os.path.join(os.getcwd(), r"d21.txt")) as f:
    for line in f:
        a, b = r.findall(line)[0]
        ingridients = a.split(' ')
        allergens = b.replace(' ', '').split(',')
        for allergen in allergens:
            all_allergens[allergen] += [set(ingridients)]
        for ing in ingridients:
            ingr_count[ing] += 1

have_allergen = set()
not_have_allergen = set()

for allergen in all_allergens:
    have_allergen |= set.intersection(*all_allergens[allergen])
    not_have_allergen |= set.union(*all_allergens[allergen])
not_have_allergen -= have_allergen

print('part 1:', sum([ingr_count[ing] for ing in not_have_allergen]), sep = '\n')

def_al = {al:set.intersection(*all_allergens[al]) - not_have_allergen for al in all_allergens}

while not all([len(def_al[al]) == 1 for al in def_al]):
    for al in def_al:
        if len(def_al[al]) == 1:
            for al2 in def_al:
                if al2 is not al:
                    def_al[al2] -= def_al[al]

print('part 2:', ','.join([list(def_al[al])[0] for al in sorted(list(def_al))]), sep = '\n')