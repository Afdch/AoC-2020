import os
from itertools import chain
from collections import defaultdict
from time import time
t = time()

#region input
with open(os.path.join(os.getcwd(), r"d24.txt")) as f:
    instructions = [line.strip() for line in f]
ni = []
for i in instructions:
    q = []
    k = 0
    while k < len(i):
        c = i[k]
        if c[0] in ['n', 's']:
            c = c + i[k+1]
            k += 1
        k += 1
        q.append(c)
    ni.append(q)
#endregion

tiles = {}

dir = {
    'sw': (-1, +1, 0),
    'w':  (-1, 0, +1),
    'nw': (0, -1, +1),
    'ne': (+1, -1, 0),
    'e':  (+1, 0, -1),
    'se': (0, +1, -1),
}

for ins in ni:
    current = (0, 0, 0) # origin
    for direction in ins:
        incr = dir[direction]
        current = tuple([current[i] + incr[i] for i in range(3)])
    if current not in tiles:
        tiles[current] = False
    tiles[current] = not tiles[current]

# part 1
print(sum([1 if tiles[x] else 0 for x in tiles]))

# part 2 game of life if we hadn't had enough
def neighbours(tile):
    return [tuple([sum(x) for x in zip(tile, dir[d])]) for d in dir]
print(time() - t)
[tiles.update({t:False}) for t in chain(*[[n for n in neighbours(x) if n if n not in tiles] for x in tiles])]
print(time() - t)
for day in range(100):
    new_pattern = defaultdict(bool)
    for tile in tiles:
        ns = neighbours(tile)
        nbs = sum([1 for x in ns if x in tiles and tiles[x]])
        if nbs == 2 or (nbs == 1 and tiles[tile]):
            new_pattern[tile] = True
            for n in ns:
                new_pattern[n] = False or new_pattern[n]

            
    tiles = new_pattern
else:
    print(day+1, sum([1 if tiles[x] else 0 for x in tiles]), len(tiles), time() - t)
