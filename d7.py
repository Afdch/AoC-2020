import os   
import re
import queue

def part1():
    rx = re.compile(r'(\w+ \w+) bag')
    bags_contain = {}


    with open(os.path.join(os.getcwd(), r"d7.txt")) as f:
        for line in f:
            bag_rule = rx.findall(line.strip())
            for i in range(1, len(bag_rule)):
                if bag_rule[i] not in bags_contain.keys():
                    bags_contain[bag_rule[i]] = [bag_rule[0]]
                else:
                    bags_contain[bag_rule[i]].append(bag_rule[0])

    shiny_gold = bags_contain['shiny gold']
    check_bags = queue.Queue()
    for bag in shiny_gold:
        check_bags.put(bag)
    while not check_bags.empty():
        check_bag = check_bags.get()
        if check_bag in bags_contain.keys():
            for new_bag in bags_contain[check_bag]:
                if new_bag not in shiny_gold:
                    shiny_gold.append(new_bag)
                    check_bags.put(new_bag)
    else:
        print(len(shiny_gold))

def part2():
    rx = re.compile(r'(\d)? ?(\w+ \w+) bag')
    bags_contain2 = {}
    with open(os.path.join(os.getcwd(), r"d7.txt")) as f:
        for line in f:
            bag_rule = rx.findall(line.strip())
            # print(bag_rule)
            for i in range(1, len(bag_rule)):                
                bg = bag_rule[i][1]
                am = bag_rule[i][0]
                r  = bag_rule[0][1]
                am = 1 if bg == 'no other' else int(am)
                if r not in bags_contain2.keys():
                    bags_contain2[r] = [(bg, am)]
                else:
                    bags_contain2[r].append((bg, am))
    # for x in bags_contain2.keys():
    #     print(bags_contain2[x])


    def check(bag: str) -> int:
        if bag in bags_contain2.keys():
            return 1 + sum([check(b)*am for b, am in bags_contain2[bag]])
        else:
            return 0

    print(check('shiny gold') - 1)
    
part2()