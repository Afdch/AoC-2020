import os
from os.path import join
import re

rules = {}
lines = []

def is_int(x) -> bool:
    try:
        int(x)
        return True
    except ValueError:
        return False

with open(os.path.join(os.getcwd(), r"d19.txt")) as f:
    le = False
    for line in f:
        l = line.strip()
        if not le:
            if not l == "":
                a, b = l.split(':')
                b = b[1:].replace('"','')
                if b in ['a', 'b']:
                    rules[int(a)] = b
                else:
                    rules[int(a)] = [tuple([int(y) for y in x.split(' ') if is_int(y)]) for x in b.split('|')]
                    # rules[int(a)] = [int(x) for x in [y.split() for y in b[1:].replace('"','').split('|')] if is_int(x)]
                    if len(rules[int(a)]) == 1:
                        rules[int(a)] = rules[int(a)][0]
                continue
            else:
                le = True
                continue
            
        lines.append(l)

# print(rules)
def plop_rule(rule):
    # print(rules[rule])
    ret = []
    for subrule in rules[rule]:
        if type(subrule) is int:
            ret.append(plop_rule(subrule))
        elif type(subrule) is tuple:
            Y = tuple([plop_rule(x) if type(x) is int else x for x in subrule])
            if all([type(x) is str for x in Y]):
                Y = ''.join(Y)
            ret.append(Y)
        elif type(subrule) is list and any([type(x) is int for x in subrule]):
            ret += [[plop_rule(sb) for sb in subrule if type(sb) is not str]]
        elif type(subrule) is str:
            ret += [subrule]
    if len(ret) > 1 and all([type(x) is str for x in ret]):
        ret = [tuple([''.join(ret)])]
    return ret[0] if len(ret) == 1 else ret

# rule 0 = 8 11
# rule 8 = 42
# rule 11 = 42 31
# ->
# rule 0 = 42 42 31
P42 = plop_rule(42)
#P31 = plop_rule(31)
print(plop_rule(0))
print(P42, len(P42))
# print(P31, len(P31))
def mk_d(P):
    dictionary = [P]
    while True:
        if all([all([type(x) is str for x in y]) for y in dictionary]):
            break
        term = dictionary.pop(0)
        for i in range(len(term)):
            if type(term[i]) is list:
                for item in term[i]:
                    new_var = term[:i] + [item] + term[i+1:]
                    dictionary.append(new_var)
                break
            elif type(term[i]) is tuple:
                dictionary.append(term[:i] + list(term[i]) + term[i+1:])
                break
    for i in range(len(dictionary)):
        dictionary[i] = ''.join(dictionary[i])
    return set(dictionary)
#print(dictionary)

P42 = mk_d(P42)
P31 = mk_d(P31)

total_match = 0
for element in lines:
    pass
print(total_match)