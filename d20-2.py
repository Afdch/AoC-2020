import os
import numpy as np

big_tiles = {}
with open(os.path.join(os.getcwd(), r"d20.txt")) as f:
    BT = 0
    for line in f:
        if (l:=line.strip()) == '':
            pass
        elif l[0] == 'T':
            BT = int(l[4:-1])
            big_tiles[BT] = []
        else:
            big_tiles[BT].append(l)

class BT:
    def __init__(self, t) -> None:
        self.number = t
        self.tile = np.array([[1 if x == '#' else 0 for x in line] for line in big_tiles[tile]])
        self.edges = {}
    
def get_edges(t) -> list:
    return set([tuple(x) for x in [ t[0,:], t[len(t)-1,:], t[:,0],  t[:,len(t[0])-1]]])
    
def get_edges_flipped(t) -> list:
    return set([tuple(x) for x in [ t[0,::-1], t[len(t)-1,::-1], t[::-1,0],  t[::-1,len(t[0])-1]]])
    
for tile in big_tiles:
    big_tiles[tile] = BT(tile)
edges = {}
for this in big_tiles:
    this_edges = get_edges(big_tiles[this].tile)
    for edge in this_edges:
        for other in big_tiles:
            if this is not other:
                if edge in get_edges(big_tiles[other].tile):
                    flipped = False
                    pass
                elif edge in get_edges_flipped(big_tiles[other].tile):
                    flipped = True
                    pass
                else:
                    continue
                
                big_tiles[this].edges[edge] = (other, flipped)
                edges[edge] = ((this, other), flipped)
                break
        else:
            # there were no other tile with the same edge
            # TODO
            big_tiles[this].edges[edge] = None

# [print(edge, edges[edge]) for edge in edges]
# part 1:
M = 1
for t in big_tiles:
    if sum([1 if big_tiles[t].edges[x] is None else 0 for x in big_tiles[t].edges]) == 2:
        M *= big_tiles[t].number
print(f"part 1 answer is {M}\n")


# part 2
c_tile = 0

for t in big_tiles:
    if sum([1 if big_tiles[t].edges[x] is None else 0 for x in big_tiles[t].edges]) == 2:
        c_tile = t
        break

print(c_tile)
# print(big_tiles[c_tile].tile, '\n')
empty_edges = [edge for edge in big_tiles[c_tile].edges if big_tiles[c_tile].edges[edge] is None]
# print(big_tiles[c_tile].edges)
# print(empty_edges)
empty_edges += [tuple((reversed(x))) for x in empty_edges]
# rotate the first corner tile
while True:
    if tuple(big_tiles[c_tile].tile[:,0]) in empty_edges:
        if tuple(big_tiles[c_tile].tile[0,:]) in empty_edges:
            break
    big_tiles[c_tile].tile = np.rot90(big_tiles[c_tile].tile)
    # print(big_tiles[c_tile].tile, '\n')
edge_right = tuple(big_tiles[c_tile].tile[:,len(big_tiles[c_tile].tile)-1])
edge_down = tuple(big_tiles[c_tile].tile[len(big_tiles[c_tile].tile)-1,:])
# print(big_tiles[c_tile].tile, '\n')
j = 0
i = 0

tile_dict = {}
tile_dict[(i, j)] = big_tiles[c_tile].tile[1:-1,1:-1] 
while True:
    fst_tile = c_tile
    while True:
        nxt = big_tiles[c_tile].edges[edge_right 
                                        if edge_right in big_tiles[c_tile].edges 
                                        else tuple(reversed(edge_right))]
        if nxt is None:
            break
        c_tile, flipped = nxt
        rot = 0
        while True:
            if tuple(big_tiles[c_tile].tile[:,0]) == edge_right:
                # print(big_tiles[c_tile].tile, '\n')
                i += 1
                tile_dict[(i, j)] = big_tiles[c_tile].tile[1:-1,1:-1] 
                break
            big_tiles[c_tile].tile = np.rot90(big_tiles[c_tile].tile)
            rot += 1
            if rot == 4:
                big_tiles[c_tile].tile = np.fliplr(big_tiles[c_tile].tile)
        edge_right = tuple(big_tiles[c_tile].tile[:,-1])
        
    # check bottom array, flip it accordingly, prepare edge down
    
    i = 0
    
    nxt = big_tiles[fst_tile].edges[edge_down if edge_down in big_tiles[fst_tile].edges else tuple(reversed(edge_down))]
    if nxt is None: 
        break
    c_tile, flipped = nxt
    
    rot = 0
    while True:
        if tuple(big_tiles[c_tile].tile[0,:]) == edge_down:
            #print(big_tiles[c_tile].tile, '\n')
            
            j += 1
            tile_dict[(i, j)] = big_tiles[c_tile].tile[1:-1,1:-1] 
            
            break
        big_tiles[c_tile].tile = np.rot90(big_tiles[c_tile].tile)
        rot += 1
        if rot == 4:
            big_tiles[c_tile].tile = np.fliplr(big_tiles[c_tile].tile)
    edge_down = tuple(big_tiles[c_tile].tile[len(big_tiles[c_tile].tile)-1,:])
    edge_right = tuple(big_tiles[c_tile].tile[:,len(big_tiles[c_tile].tile)-1])

max_i, max_j = [max(x) for x in zip(*list(tile_dict.keys()))]

sea = np.concatenate([np.concatenate([tile_dict[(i, j)] for i in range(max_i + 1)],axis = 1) for j in range(max_j + 1)])
monster = np.array([[1 if x == '#' else 0 for x in line] 
                    for line in    ['                  # ',
                                    '#    ##    ##    ###',
                                    ' #  #  #  #  #  #   ']])
sea_masked = np.empty_like(sea)
sea_masked[:,:] = sea
for k in range(8):
    for j in range(len(sea) - len(monster) + 1):
        for i in range(len(sea[0]) - len(monster[0]) + 1):
            slice = sea[j:j+len(monster),i:i+len(monster[0]+1)]
            if np.array_equal(slice*monster, monster):
                for n in range(len(monster)):
                    for m in range(len(monster[0])):
                        if monster[n,m] == 1:
                            sea_masked[j+n,i+m] = 0
    sea = np.rot90(sea)
    sea_masked = np.rot90(sea_masked)
    if k == 3:
        sea = np.fliplr(sea)
        sea_masked = np.fliplr(sea_masked)

    
print(np.sum(sea_masked)) # 2351 is too high

from PIL import Image
coeff = 0.3
im = Image.fromarray((sea*coeff + (1-coeff)*np.logical_xor(sea, sea_masked))*255)
sf = 10
im = Image.Image.resize(im, (len(sea)*sf, len(sea)*sf), resample=Image.NEAREST)
im.show()