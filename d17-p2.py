import os
import itertools
from time import time

def cubeit(dim, cycles):
    cubes = {}

    with open(os.path.join(os.getcwd(), r"d17.txt")) as f:
        j = 0
        for line in f:
            i = 0
            for char in line.strip():
                if char == '#':
                    cubes[tuple([i, j] + [0] * (dim - 2))] = 1
                i += 1
            j += 1

    dcs = [c for c in list(itertools.product(range(-1, 2), repeat = dim)) if not all(c[n] == 0 for n in range(dim))]

    def neighboursA(tile):    
        return [tuple([tile[i] + dc[i] for i in range(dim)]) for dc in dcs]

    def neighboursB(tile):    
        return sum([cubes[cube] for cube in neighboursA(tile) if cube in cubes.keys()])



    for cycle in range(cycles):
        n_cubes = {} # next state for the cubes
        
        # only need to check the neigbours of the curren active cubes
        B = set(itertools.chain.from_iterable([neighboursA(cube) for cube in cubes]))
        
        for cube in B:
            n = neighboursB(cube)
            if n == 3 or (n == 2 and cube in cubes.keys() and cubes[cube] == 1):
                n_cubes[cube] = 1 
        cubes = n_cubes
        print(cycle + 1, len(cubes))


t = time()
cubeit(3, 6)
print(time() - t)
t = time()
cubeit(4, 6)
print(time() - t)