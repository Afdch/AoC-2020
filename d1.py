import os
from itertools import combinations
from functools import reduce

ints = []
with open(os.path.join(os.getcwd(), r"d1.txt")) as f:
    ints = set([int(line.strip()) for line in f])

def solution(amount):
    for combination in combinations(ints, amount):
        if sum(combination) == 2020:
            print(reduce(lambda x, y: x*y, (combination)))
            break

#part 1
solution(2)
#part 2
solution(3)