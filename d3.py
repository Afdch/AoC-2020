import os
from aoc import Maze
from functools import reduce


with open(os.path.join(os.getcwd(), r"d3.txt")) as f:
    maze = Maze(f)

def count_trees(dx, dy):
    x = 0
    y = 0

    trees = 0

    while y <= maze.maxY:
        if maze.isWall((x, y)):
            trees += 1
        x = (x + dx) 
        if x > maze.maxX:
            x -= maze.maxX + 1
        y += dy
    else:
        return(trees)
    
# part1
print(count_trees(3, 1))

# part2
slope_trees = [count_trees(x, y) for x, y in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]]
print(reduce(lambda x, y: x*y, (slope_trees)))