import os


with open(os.path.join(os.getcwd(), r"d5.txt")) as f:
    seats = []
    check = 0
    for line in f:
        minrow, maxrow = 0, 127
        mincol, maxcol = 0, 7
        for character in line.strip():
            if character == 'F':
                maxrow = (maxrow + minrow + 1) // 2 - 1
            elif character == 'B':
                minrow = (maxrow + minrow + 1) // 2
            elif character == 'L':
                maxcol = (maxcol + mincol + 1) // 2 - 1
            elif character == 'R':
                mincol = (maxcol + mincol + 1) // 2
            
        seat = minrow * 8 + mincol
        seats.append(seat)
        check = max(check, seat)
    print(check)
    
    seats.sort()
    for i in range(len(seats)):
        if seats[i] + 2 == seats[i + 1]:
            print(seats[i] + 1)
            break