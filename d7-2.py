import os   
import re
import networkx as nx
import matplotlib.pyplot as plt
from networkx.algorithms.dag import ancestors, descendants

bags = nx.DiGraph()
# reading file
with open(os.path.join(os.getcwd(), r"d7.txt")) as f:
    for line in f:
        bag_rule = re.compile(r'(\d)? ?(\w+ \w+) bag').findall(line.strip())
        # print(bag_rule)
        for i in range(1, len(bag_rule)):                
            bg = bag_rule[i][1]
            if bg == 'no other':
                continue
            am = bag_rule[i][0]
            r  = bag_rule[0][1]            
            bags.add_edge(r, bg, subbags=int(am))
    
    # part 1    
    print(len(ancestors(bags, 'shiny gold')))
    
    # part 2
    bags = nx.relabel_nodes(bags, {x:x.replace(' ', '\n') for x in bags.nodes()}, copy = True)
    bagss = nx.Graph.subgraph(bags, descendants(bags,'shiny\ngold') | set(['shiny\ngold']))
    def subbags(bag:str) -> int:
        return 1 + sum(bagss[bag][subbag]['subbags']*subbags(subbag) for subbag in bagss[bag])
    print(subbags('shiny\ngold') - 1)
    
    # additional printing
    node_colours = list(['#e36856' if x != 'shiny\ngold' else '#ffd700' for x in bagss.nodes])
    nodePos = nx.kamada_kawai_layout(bagss, weight='subbags')
    with plt.style.context('dark_background'):
        plt.figure()
        plt.style.use('Solarize_Light2')
        # ax = plt.gca()
        # ax.set_facecolor('#10112d')
        nx.draw_networkx(
            bagss, 
            pos = nodePos,
            node_shape = '8',
            node_size = 1000,
            with_labels = True,
            arrowsize = 20,
            node_color = node_colours,
            # font_color = '#e6dfcf',
            edge_color = '#e36856'
        )
        
        nx.draw_networkx_edge_labels(
            bagss,
            pos = nodePos, 
            edge_labels = nx.get_edge_attributes(bagss, 'subbags'),
            alpha = 0.9,
            horizontalalignment = 'center'
        )
        plt.show()