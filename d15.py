numbers = [0,12,6,13,20,1,17]
num_times = {numbers[i]:i for i in range(len(numbers) - 1)}


last_number = numbers[-1]
# part 1
# while len(numbers) < 2020:
for len_numbers in range(len(numbers) - 1, 30000000 - 1):
    if last_number not in num_times.keys():
        next_number = 0
    else:
        next_number = len_numbers - num_times[last_number]
    num_times[last_number] = len_numbers
    last_number = next_number
    

print(last_number)
print(len(num_times.keys()))