import os
import aoc
import printd

with open(os.path.join(os.getcwd(), r"d11.txt")) as f:
    seats = aoc.Maze(f)

def get_cardinal_seats(seat, seat_state):
    x, y = seat
    # part 1:
    # return sum([1 for (nX, nY) in [(x+1, y+1), (x+1, y), (x+1, y-1),
    #                                 (x, y+1), (x, y-1),
    #                                 (x-1, y+1), (x-1, y), (x-1, y-1)] 
    #                         if 0 <= nX <= seats.maxX 
    #                         and 0 <= nY <= seats.maxY
    #                         and seat_state[(nX, nY)] == '#'])
    # part 2:
    occupied = 0
    for dx, dy in [(1, 1), (1, 0), (1, -1), (0, 1), (0, -1), (-1, +1), (-1, 0), (-1, -1)]:
        i = 1
        while True:
            nX, nY = (x + i*dx, y + i*dy)
            if not(0 <= nX <= seats.maxX and 0 <= nY <= seats.maxY):
                break
            if (Z:= seat_state[(nX, nY)]) == '.':
                i += 1
                continue
            elif Z == "#":
                occupied += 1
                break
            else:
                break
    return occupied
            
    

def mutate(seat_state: dict) -> dict:
    new_state = {}
    for x, y in seat_state.keys():
        neighbours = get_cardinal_seats((x, y), seat_state)
        if seat_state[(x, y)] == 'L' and neighbours == 0:
            new_state[(x, y)] = '#'
        elif seat_state[(x, y)] == '#' and neighbours >= 5 : # neighbours >= 4 for part 1
            new_state[(x, y)] = 'L'
        else:
            new_state[(x, y)] = seat_state[(x, y)]
    return new_state

tileset = {x: x for x in seats.tiles.values()}
tileset['#'] = '#'

new_state = seats.tiles
# printd.printd(new_state, tileset, False)
seat_states = [new_state]




scalefactor = 8
Images = []
while True:
    new_state = mutate(new_state)
    # print(len(seat_states))
    # printd.printd(new_state, tileset, False)
    Images.append(printd.mkimg(new_state, scalefactor, False))
    if new_state in seat_states:
        print(len(seat_states))
        sum_occupied = sum([1 for x in new_state.keys() if new_state[x] == "#"])
        # print(sum_occupied)
        printd.savegif(Images, 'd11', 1)
        break
    else:
        seat_states.append(new_state)
        Images.append(printd.mkimg(new_state, scalefactor, False))