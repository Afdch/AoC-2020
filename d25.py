def transform(pub_key, loop_size):
    return pow(pub_key, loop_size, 20201227)

def find_loop_size(public_key):
    loop_size = 0
    value = 1
    while (value := value * 7 % 20201227) != public_key:
        loop_size += 1
    return loop_size + 1

card_pub_key = 16915772
door_pub_key = 18447943

print(transform(door_pub_key, find_loop_size(card_pub_key)))
print(transform(card_pub_key, find_loop_size(door_pub_key))) # sanity check