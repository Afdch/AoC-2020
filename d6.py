import os   

with open(os.path.join(os.getcwd(), r"d6.txt")) as f:
    groups = ("".join([x for x in f])).split('\n\n')
    # part 1:
    print(sum([len(set(x.replace('\n', ''))) for x in groups]))
    # part 2
    print(sum([sum([1 for question in set("".join(x for x in group)) 
                    if "".join(x for x in group).count(question) == len(group)]) 
                        for group in (x.split('\n') for x in groups)]))