import os   
import asm

comp = asm.computer()

with open(os.path.join(os.getcwd(), r"d8.txt")) as f:
    comp.read_instruction_list(f)

def part1():
    executed = set()
    comp.accumulator = 0
    comp.pointer = 0
    while comp.pointer not in executed:
        if comp.pointer >= len(comp.instruction_list):
            return False
        executed |= set([comp.pointer])
        comp.exec_instr()
    else:
        return executed


executed = part1(True)
possible_errors = [x for x in executed if comp.instruction_list[x][0] in ['jmp', 'nop']]
print(f'Part 1 answer: {comp.accumulator}')

pointer = 0
for err in possible_errors:
    A, B = (comp.instruction_list[err][0], comp.instruction_list[err][1])
    C = 'nop' if A == 'jmp' else 'jmp'
    comp.instruction_list[err] = (C, B)
    if not part1():
        break
    comp.instruction_list[err] = (A, B)
else:
    print('something broke')
print(f'Part 2 answer: {comp.accumulator}')