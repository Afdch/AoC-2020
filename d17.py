import os

cubes = {}
d = (-1, 0, 1)
with open(os.path.join(os.getcwd(), r"d17.txt")) as f:
    j = 0
    for line in f:
        i = 0
        for char in line.strip():
            s = 1 if char == '#' else 0
            cubes[(i, j, 0)] = s
            i += 1
        j += 1

def neighbours(tile):
        x, y, z = tile
        return sum([cubes[(x + dx, y + dy, z + dz)]
                    for dx in d for dy in d for dz in d 
                    if not dx == dy == dz == 0 
                    and minX <= x + dx <= maxX 
                    and minY <= y + dy <= maxY 
                    and minZ <= z + dz <= maxZ])
            

maxX = max([x for x, _, _ in cubes.keys()])
maxY = max([y for _, y, _ in cubes.keys()])
maxZ = max([z for _, _, z in cubes.keys()])
minX, minY, minZ = 0, 0, 0

for cycle in range(6):
    n_cubes = {} # next state for the cubes
    # region check if we need to expand the cubes
    
    if any([cubes[(minX, y, z)] == 1 for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1)]):
        minX -= 1
        for cube in [(minX, y, z) for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1)]:
            cubes[cube] = 0            
    if any([cubes[(maxX, y, z)] == 1 for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1)]):
        maxX += 1
        for cube in [(maxX, y, z) for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1)]:
            cubes[cube] = 0
    if any([cubes[(x, minY, z)] == 1 for x in range(minX, maxX + 1) for z in range(minZ, maxZ + 1)]):
        minY -= 1
        for cube in [(x, minY, z) for x in range(minX, maxX + 1) for z in range(minZ, maxZ + 1)]:
            cubes[cube] = 0
    if any([cubes[(x, maxY, z)] == 1 for x in range(minX, maxX + 1) for z in range(minZ, maxZ + 1)]):
        maxY += 1
        for cube in [(x, maxY, z) for x in range(minX, maxX + 1) for z in range(minZ, maxZ + 1)]:
            cubes[cube] = 0
    if any([cubes[(x, y, minZ)] == 1 for x in range(minX, maxX + 1) for y in range(minY, maxY + 1)]):
        minZ -= 1
        for cube in [(x, y, minZ) for x in range(minX, maxX + 1) for y in range(minY, maxY + 1)]:
            cubes[cube] = 0
    if any([cubes[(x, y, maxZ)] == 1 for x in range(minX, maxX + 1) for y in range(minY, maxY + 1)]):
        maxZ += 1
        for cube in [(x, y, maxZ) for x in range(minX, maxX + 1) for y in range(minY, maxY + 1)]:
            cubes[cube] = 0
    #endregion
    for cube in [(x, y, z) for x in range(minX, maxX + 1) for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1)]:
        if cubes[cube] == 1:
            n_cubes[cube] = 1 if neighbours(cube) in [2, 3] else 0
        else:
            n_cubes[cube] = 1 if neighbours(cube) == 3 else 0

    cubes = n_cubes
print(sum(list(cubes.values())), '\n')
    # for z in range(minZ, maxZ + 1):
    #     print(f'\nz={z}')
    #     for y in range(minY, maxY + 1):
    #         print("".join([str(cubes[(x, y, z)]) for x in range(minX, maxX + 1)]))
