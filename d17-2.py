import os

cubes = {}
d = (-1, 0, 1)
with open(os.path.join(os.getcwd(), r"d17.txt")) as f:
    j = 0
    for line in f:
        i = 0
        for char in line.strip():
            s = 1 if char == '#' else 0
            cubes[(i, j, 0, 0)] = s
            i += 1
        j += 1

def neighbours(tile):
        x, y, z, w = tile
        return sum([cubes[(x + dx, y + dy, z + dz, w + dw)]
                    for dx in d for dy in d for dz in d for dw in d
                    if not dx == dy == dz == dw == 0 
                    and minX <= x + dx <= maxX 
                    and minY <= y + dy <= maxY 
                    and minZ <= z + dz <= maxZ
                    and minW <= w + dw <= maxW])
            

maxX = max([x for x, _, _, _ in cubes.keys()])
maxY = max([y for _, y, _, _ in cubes.keys()])
maxZ = max([z for _, _, z, _ in cubes.keys()])
maxW = max([w for _, _, _, w in cubes.keys()])
minX, minY, minZ, minW = 0, 0, 0, 0

for cycle in range(6):
    n_cubes = {} # next state for the cubes
    # region check if we need to expand the cubes
    # x
    if any([cubes[(minX, y, z, w)] == 1 for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1) for w in range(minW, maxW + 1)]):
        minX -= 1
        for cube in [(minX, y, z, w) for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1) for w in range(minW, maxW + 1)]:
            cubes[cube] = 0            
    if any([cubes[(maxX, y, z, w)] == 1 for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1) for w in range(minW, maxW + 1)]):
        maxX += 1
        for cube in [(maxX, y, z, w) for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1) for w in range(minW, maxW + 1)]:
            cubes[cube] = 0
    # y
    if any([cubes[(x, minY, z, w)] == 1 for x in range(minX, maxX + 1) for z in range(minZ, maxZ + 1) for w in range(minW, maxW + 1)]):
        minY -= 1
        for cube in [(x, minY, z, w) for x in range(minX, maxX + 1) for z in range(minZ, maxZ + 1) for w in range(minW, maxW + 1)]:
            cubes[cube] = 0
    if any([cubes[(x, maxY, z, w)] == 1 for x in range(minX, maxX + 1) for z in range(minZ, maxZ + 1) for w in range(minW, maxW + 1)]):
        maxY += 1
        for cube in [(x, maxY, z, w) for x in range(minX, maxX + 1) for z in range(minZ, maxZ + 1) for w in range(minW, maxW + 1)]:
            cubes[cube] = 0
    # z
    if any([cubes[(x, y, minZ, w)] == 1 for x in range(minX, maxX + 1) for y in range(minY, maxY + 1) for w in range(minW, maxW + 1)]):
        minZ -= 1
        for cube in [(x, y, minZ, w) for x in range(minX, maxX + 1) for y in range(minY, maxY + 1) for w in range(minW, maxW + 1)]:
            cubes[cube] = 0
    if any([cubes[(x, y, maxZ, w)] == 1 for x in range(minX, maxX + 1) for y in range(minY, maxY + 1) for w in range(minW, maxW + 1)]):
        maxZ += 1
        for cube in [(x, y, maxZ, w) for x in range(minX, maxX + 1) for y in range(minY, maxY + 1) for w in range(minW, maxW + 1)]:
            cubes[cube] = 0
    # w
    if any([cubes[(x, y, z, minW)] == 1 for x in range(minX, maxX + 1) for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1)]):
        minW -= 1
        for cube in [(x, y, z, minZ) for x in range(minX, maxX + 1) for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1)]:
            cubes[cube] = 0
    if any([cubes[(x, y, z, maxW)] == 1 for x in range(minX, maxX + 1) for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1)]):
        maxW += 1
        for cube in [(x, y, z, maxZ) for x in range(minX, maxX + 1) for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1)]:
            cubes[cube] = 0
    #endregion
    for cube in [(x, y, z, w) for x in range(minX, maxX + 1) for y in range(minY, maxY + 1) for z in range(minZ, maxZ + 1) for w in range(minW, maxW + 1)]:
        if cubes[cube] == 1:
            n_cubes[cube] = 1 if neighbours(cube) in [2, 3] else 0
        else:
            n_cubes[cube] = 1 if neighbours(cube) == 3 else 0

    cubes = n_cubes
print(sum(list(cubes.values())), '\n')
